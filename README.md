 Setting up your shell environment
 ===================================

[TOC]

## Introduction

 If you are a developer using an Unix environment, chances are you are using quite extensively your terminal.

 The point of this lightning talk is to convince you to take a bit of time setting it up properly to ease your life later.

 **Disclaimer 1:** I will present my own set up, but my point is *not* to tell you to use exactly the same. There are many options available (see for instance several ones just for the zsh shell [here](https://github.com/agkozak/zsh-z)) and my needs are probably not the same as yours.

 **Disclaimer 2:** I'm not a specialist of shell configurations and have barely scratched the surface; my use remains rather minimalist compared to what some colleagues do.

## Zsh

I have used for many years now as primary shell zsh instead of bash, initially following advice from a colleague.

I can't tell you every pro and con of each if these shells, but zsh is deemed much more customizable than bash.

By default:

- Linux distro I'm using (Ubuntu and Fedora) come with bash shell by default.
- Fairly recently (around 2019), Apple made the switch to zsh by default (which doesn't mean bash no longer works on macOS).

In the following I assume zsh is installed; it might require a command such as:

````shell
sudo apt install zsh
````


## Oh-my-zsh

I have used [Oh-my-zsh](https://ohmyz.sh) much more recently; it provides many things out of the box and is highly configurable (as we shall see shortly).

As specified on their web page, to install it you have to run:

````shell
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
````

**Important:** you will be prompted to choose whether to use zsh by default; you however need to close and reopen the session (at least for Ubuntu) to ensure terminals are by default zsh ones.

If you edit the file `~/.zshrc`, you'll see oh-my-zsh provided it completely with many comments aimed at helping you customizing it to your need.

### Theme

The theme is basically what your prompt will look like.

There are many available off the shelf in oh-my-zsh, but I ended up choosing one that is not: the [spaceship theme](https://github.com/spaceship-prompt/spaceship-prompt).

Even so the installation is easy:

````shell
git clone https://github.com/spaceship-prompt/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt" --depth=1
ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"
````
 
 After that, you just have to set it up in the `.zshrc`:

 ````shell
 ZSH_THEME="spaceship"
 ````

### Plugins


You may activate one or several [plugins](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins) that will unlock new functionalities and/or aliases.

Beware though: as indicated in the `.zshrc`, they will slow down the opening of a new terminal.

On my laptop I chose the following ones:

````
plugins=(z colored-man-pages colorize macos)
````

#### Git  

I have removed the default [git](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/git) one: I do not use any of the aliases defined there.

#### z

The [z](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/z) plugin is very handy to navigate through the folders you visit often.

If I type 

````shell
z build
````

the terminal will move to a directory with `build` in its name that was visited recently and/or often (and do nothing if none found).

z features several flags to fine tune its behaviour: for instance

````shell
z -l build
````

will print the possible choices found but not move.

#### colored-man-pages

Does exactly what is written on the can!

#### colorize

Defines a new `ccat` command which will do a colorized `cat`.

You need to install either `pygmentize` or `chroma` to make it work, for instance through

````shell
sudo apt install python3-pygments
````

in Ubuntu.

### Your own customized configuration

If you have already dabbled with shells, you have probably already appended stuff (commands, `PATH` extension and so forth) in a file such as `.bash_profile` or `.zshrc`.

Oh-my-zsh provides a rather nice way to add new content neatly: each file added in directory `~/.oh-my-zsh/custom` with a `.zsh` extension will be interpreted as an extension of your configuration. The `spaceship` extension added earlier was for that matter installed there as well (in subdirectory `themes`).

I find that using different files make the intent clearer and less confuse than adding lots of stuff directly at the end of a `.zshrc` files.

For instance some of the files I have defined for my laptop are:

#### history_per_tab.zsh

````shell
# Keep a separate history per page
MYTTY=`tty`
HISTFILE=$HOME/.zsh_history_`basename $MYTTY`
````

This can be done with bash shell as well (see [this thread](https://apple.stackexchange.com/questions/41001/how-can-i-get-the-terminal-to-keep-a-separate-history-per-tab-window))

#### rm.zsh

````shell
# To avoid file removal without confirmation
alias rm='rm -i'
````

#### conda.zsh

````shell
eval "$(conda "shell.$(basename "${SHELL}")" hook)"
````

#### numkeys.zsh

Trick to be able to use easily numpad with macOS (default behaviour for my keyboard layout requires to press some keys before using some of the signs).

````shell
# Keypad
# 0 . Enter
bindkey -s "^[Op" "0"
bindkey -s "^[On" "."
bindkey -s "^[OM" "^M"
# 1 2 3
bindkey -s "^[Oq" "1"
bindkey -s "^[Or" "2"
bindkey -s "^[Os" "3"
# 4 5 6
bindkey -s "^[Ot" "4"
bindkey -s "^[Ou" "5"
bindkey -s "^[Ov" "6"
# 7 8 9
bindkey -s "^[Ow" "7"
bindkey -s "^[Ox" "8"
bindkey -s "^[Oy" "9"
# + -  * / =
bindkey -s "^[Ol" "+"
bindkey -s "^[Om" "-"
bindkey -s "^[Oj" "*"
bindkey -s "^[Oo" "/"
bindkey -s "^[OX" "="
````

#### shell_scripts.zsh

````shell
export PATH=${PATH}:/Users/sgilles/Codes/Scripts/
````

#### autocorrect.zsh

````shell
setopt correct
````